﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using OAuth2.Core.Models;
using OAuth2.Core.Helpers;
using FacebookDemo.Database;

namespace FacebookDemo.Helpers
{
    public class UserManager : IUserManager
    {
        public static UserManager Instance
        {
            get
            {
                return new UserManager();
            }
        }

        public Response ProcessAuthResponse(AuthResponseResult result)
        {
            if (result)
            {
                Db.Transact(() =>
                {
                    var u = result.User;
                    var t = result.Token;

                    var oau = Db.SQL<OAUser>("SELECT oau FROM FacebookDemo.Database.OAUser oau").FirstOrDefault();

                    if (null != oau)
                    {
                        oau.Token.Delete();
                        oau.Delete();
                    }
                        
                    var token = new OAToken()
                    {
                        AccessToken = t.AccessToken,
                        RefreshToken = t.RefreshToken,
                        Expires = t.Expires,
                        Issued = t.Issued
                    };

                    var oauser = new OAUser()
                    {
                        Provider = u.Provider,
                        ProviderId = u.ProviderId,
                        Name = u.Name,
                        Email = u.Email,
                        Gender = u.Gender,
                        Locale = u.Locale,
                        Picture = u.Picture,
                        Profile = u.Profile,
                        Token = token
                    };
                });

                return ResponseHelper.Redirect("/facebookdemo");
            }

            return Response.FromStatusCode(401);
        }
    }
}
