﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using OAuth2.Core.Models;
using FacebookDemo.Database;

namespace FacebookDemo.Helpers
{
    public class TokenManager : ITokenManager
    {
        protected OAUser user;

        public TokenManager(OAUser user)
        {
            this.user = user;
        }

        public IToken GetToken()
        {
            return this.user.Token;
        }

        public void SetToken(IToken token)
        {
            Db.Transact(() =>
            {
                user.Token.AccessToken = token.AccessToken;

                if (null != token.RefreshToken)
                    user.Token.RefreshToken = token.RefreshToken;

                user.Token.Expires = token.Expires;
            });
        }
    }
}
