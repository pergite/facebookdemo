﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OAuth2.Core.Models;

namespace FacebookDemo.Helpers
{
    public class Settings : ISettings
    {
        public string ServiceName { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }

    public class SettingsManager : ISettingsManager
    {
        public static SettingsManager Instance
        {
            get
            {
                return new SettingsManager();
            }
        }

        public ISettings GetSettings(string serviceName)
        {
            if (serviceName.ToLower() != "facebook")
                return null;

            return new Settings()
            {
                ServiceName = "facebook",
                ClientID = "354325051656896",
                ClientSecret = "765fe89aa4d980c23ef8acc6cc7ff9d9"
            };
        }
    }
}
