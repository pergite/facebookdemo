﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;

using OAuth2.Core.Models;


namespace FacebookDemo.Database
{
    [Database]
    public class OAToken : IToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime Issued { get; set; }
        public DateTime Expires { get; set; }
    }

    [Database]
    public class OAUser
    {
        public string Provider { get; set; }
        public string ProviderId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Profile { get; set; }
        public string Picture { get; set; }
        public string Gender { get; set; }
        public string Locale { get; set; }

        public OAToken Token { get; set; }
    }
}
