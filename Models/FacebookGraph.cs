﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookDemo.Models
{
    //    {"data":[{"message":"Ska bli kul att testa :-)","created_time":"2017-12-14T21:00:23+0000","id":"10212632561462679_10213200491980587"}

    public class UserFeed
    {
        public Post[] data;
    }

    public class Post
    {
        public string id;
        public string message;
        public DateTime created_time;
    }
}
