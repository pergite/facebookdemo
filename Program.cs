﻿using System;
using System.Linq;
using Starcounter;

using OAuth2.Core.Handlers;
using OAuth2.Core.Services;

using FacebookDemo.Database;
using FacebookDemo.Helpers;
using FacebookDemo.Views;
using FacebookDemo.Models;

namespace FacebookDemo
{
    class Program
    {
        static void Main()
        {
            Application.Current.Use(new HtmlFromJsonProvider());
            Application.Current.Use(new PartialToStandaloneHtmlProvider());

            OAuth2Flow.Register(new Uri("/facebookdemo/", UriKind.Relative), SettingsManager.Instance, UserManager.Instance);

            Handle.GET("/facebookdemo", () =>
            {
                var oauser = Db.SQL<OAUser>("SELECT oau FROM FacebookDemo.Database.OAUser oau").FirstOrDefault();

                if (null == oauser)
                {
                    return Self.GET("/facebookdemo/facebook/signin?scope=user_posts");
                }

                var page = new FacebookDemoPage()
                {
                    Name = oauser.Name
                };

                // get users last posts from Facebook 

                var facebook = Factory.Get(SettingsManager.Instance.GetSettings("facebook"), new TokenManager(oauser));

                var feed = facebook.Get<UserFeed>($"https://graph.facebook.com/me/feed?limit=10");

                if (null != feed.data)
                {
                    foreach (var post in feed.data)
                    {
                        var feedItem = page.Feed.Add();
                        feedItem.Message = post.message;
                        feedItem.Date = post.created_time.ToString("yyyy-MM-dd HH:mm");
                    }
                }

                return page;
            });
        }
    }
}
